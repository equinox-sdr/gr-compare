
from gnuradio import gr
import compare_swig
import weakref

class chain_connect(gr.hier_block2):
    """
    docstring for block chain_connect
    """
    def __init__(self, msg_size=32, nblocks=4):
        gr.hier_block2.__init__(self,
            "chain_connect",
            gr.io_signature(0, 0, 0),  # Input signature
            gr.io_signature(0, 0, 0)) # Output signature

        self.message_port_register_hier_in("in");
        self.message_port_register_hier_out("out");
        
        self.blocks = [];
        
        self.blocks.append(compare_swig.in_out(1, msg_size))
        self.msg_connect(self, "in", self.blocks[0], "in")
        # Define blocks and connect them
        for i in range(1, nblocks):
            self.blocks.append(compare_swig.in_out(1, msg_size))
            self.msg_connect(self.blocks[i - 1], "out", self.blocks[i], "in")
        
        self.msg_connect(self.blocks[nblocks - 1], "out", weakref.proxy(self), "out")
            
