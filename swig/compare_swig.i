/* -*- c++ -*- */

#define COMPARE_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "compare_swig_doc.i"

%{
#include "compare/source.h"
#include "compare/in_out.h"
#include "compare/sink.h"
#include "compare/stream_source.h"
#include "compare/stream_sink.h"
%}


%include "compare/source.h"
GR_SWIG_BLOCK_MAGIC2(compare, source);
%include "compare/in_out.h"
GR_SWIG_BLOCK_MAGIC2(compare, in_out);
%include "compare/sink.h"
GR_SWIG_BLOCK_MAGIC2(compare, sink);
%include "compare/stream_source.h"
GR_SWIG_BLOCK_MAGIC2(compare, stream_source);
%include "compare/stream_sink.h"
GR_SWIG_BLOCK_MAGIC2(compare, stream_sink);
