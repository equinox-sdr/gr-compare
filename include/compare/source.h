/* -*- c++ -*- */
/* 
 * Copyright 2018, 2019 Manolis Surligas <surligas@gmail.com>
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_COMPARE_SOURCE_H
#define INCLUDED_COMPARE_SOURCE_H

#include <compare/api.h>
#include <gnuradio/block.h>

namespace gr
{
namespace compare
{

/*!
 * \brief <+description of block+>
 * \ingroup compare
 *
 */
class COMPARE_API source : virtual public gr::block
{
public:
  typedef boost::shared_ptr<source> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of compare::source.
   *
   * To avoid accidental use of raw pointers, compare::source's
   * constructor is in a private implementation
   * class. compare::source::make is the public interface for
   * creating new instances.
   */
  static sptr
  make (size_t nports, size_t msg_size, size_t delayus);
};

} // namespace compare
} // namespace gr

#endif /* INCLUDED_COMPARE_SOURCE_H */

