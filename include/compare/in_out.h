/* -*- c++ -*- */
/* 
 * Copyright 2018, 2019 Manolis Surligas <surligas@gmail.com>
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_COMPARE_IN_OUT_H
#define INCLUDED_COMPARE_IN_OUT_H

#include <compare/api.h>
#include <gnuradio/block.h>

namespace gr
{
namespace compare
{

/*!
 * \brief <+description of block+>
 * \ingroup compare
 *
 */
class COMPARE_API in_out : virtual public gr::block
{
public:
  typedef boost::shared_ptr<in_out> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of compare::in_out.
   *
   * To avoid accidental use of raw pointers, compare::in_out's
   * constructor is in a private implementation
   * class. compare::in_out::make is the public interface for
   * creating new instances.
   */
  static sptr
  make (size_t nports, size_t msg_size);
};

} // namespace compare
} // namespace gr

#endif /* INCLUDED_COMPARE_IN_OUT_H */

