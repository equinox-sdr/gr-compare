/* -*- c++ -*- */
/* 
 * Copyright 2018, 2019 Manolis Surligas <surligas@gmail.com>
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "source_impl.h"
#include <chrono>

namespace gr
{
namespace compare
{

source::sptr
source::make (size_t nports, size_t msg_size, size_t delayus)
{
  return gnuradio::get_initial_sptr (
      new source_impl (nports, msg_size, delayus));
}

source_impl::source_impl (size_t nports, size_t msg_size, size_t delayus) :
        gr::block ("source",
                   io_signature::make (0, 0, 0),
                   io_signature::make (0, 0, 0)),
        d_nports (nports),
        d_msg_size (msg_size),
        d_delayus(delayus),
        d_cnt (0),
        d_running (true)
{
  if(d_msg_size < sizeof(std::chrono::high_resolution_clock::time_point)) {
    throw std::invalid_argument("Message size is too small");
  }

  d_buffer = new uint8_t[msg_size];
  message_port_register_out (pmt::mp ("out"));
  d_thread = boost::shared_ptr<boost::thread> (
      new boost::thread (boost::bind (&source_impl::send_msgs, this)));
}

void
source_impl::send_msgs ()
{
  if (d_delayus > 0) {
    while (d_running) {
      std::chrono::high_resolution_clock::time_point now =
          std::chrono::high_resolution_clock::now();
      memcpy(d_buffer, &now, sizeof(now));
      message_port_pub (pmt::mp ("out"), pmt::make_blob (d_buffer, d_msg_size));
      d_cnt++;
      boost::this_thread::sleep_for(boost::chrono::microseconds(d_delayus));
    }
  }
  else {
    while (d_running) {
      std::chrono::high_resolution_clock::time_point now =
          std::chrono::high_resolution_clock::now();
      memcpy(d_buffer, &now, sizeof(now));
      message_port_pub (pmt::mp ("out"), pmt::make_blob (d_buffer, d_msg_size));
      d_cnt++;
    }
  }
}

/*
 * Our virtual destructor.
 */
source_impl::~source_impl ()
{
  d_running = false;
  d_thread->join();
  delete [] d_buffer;
}

} /* namespace compare */
} /* namespace gr */

