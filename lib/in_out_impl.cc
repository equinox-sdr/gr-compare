/* -*- c++ -*- */
/* 
 * Copyright 2018, 2019 Manolis Surligas <surligas@gmail.com>
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "in_out_impl.h"
#include <chrono>

namespace gr
{
namespace compare
{

in_out::sptr
in_out::make (size_t nports, size_t msg_size)
{
  return gnuradio::get_initial_sptr (new in_out_impl (nports, msg_size));
}

/*
 * The private constructor
 */
in_out_impl::in_out_impl (size_t nports, size_t msg_size) :
        gr::block ("in_out",
                   gr::io_signature::make (0, 0, 0),
                   gr::io_signature::make (0, 0, 0)),
        d_nports (nports),
        d_msg_size(msg_size)
{
  if(d_msg_size < sizeof(std::chrono::high_resolution_clock::time_point)) {
    throw std::invalid_argument("Message size is too small");
  }
  d_buffer = new uint8_t[msg_size];
  message_port_register_in (pmt::mp ("in"));
  message_port_register_out (pmt::mp ("out"));

  set_msg_handler (pmt::mp ("in"),
                   boost::bind (&in_out_impl::handler, this, _1));
}

void
in_out_impl::handler (pmt::pmt_t m)
{
  memcpy (d_buffer, pmt::blob_data (m), d_msg_size);
  message_port_pub (pmt::mp ("out"), pmt::make_blob (d_buffer, d_msg_size));
}

/*
 * Our virtual destructor.
 */
in_out_impl::~in_out_impl ()
{
}

} /* namespace compare */
} /* namespace gr */

