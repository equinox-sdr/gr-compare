/* -*- c++ -*- */
/* 
 * Copyright 2018, 2019 Manolis Surligas <surligas@gmail.com>
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "sink_impl.h"
#include <chrono>

namespace gr
{
namespace compare
{

sink::sptr
sink::make (size_t nports, size_t msg_size)
{
  return gnuradio::get_initial_sptr (new sink_impl (nports, msg_size));
}

/*
 * The private constructor
 */
sink_impl::sink_impl (size_t nports, size_t msg_size) :
        gr::block ("sink",
                   gr::io_signature::make (0, 0, 0),
                   gr::io_signature::make (0, 0, 0)),
        d_msg_size(msg_size),
        d_cnt (0),
        d_delay(0.0)
{
  if(d_msg_size < sizeof(std::chrono::high_resolution_clock::time_point)) {
    throw std::invalid_argument("Message size is too small");
  }
  d_buffer = new uint8_t[msg_size];
  message_port_register_in (pmt::mp ("in"));
  set_msg_handler (pmt::mp ("in"), boost::bind (&sink_impl::handler, this, _1));
}

void
sink_impl::handler (pmt::pmt_t m)
{
  std::chrono::high_resolution_clock::time_point start;
  std::chrono::high_resolution_clock::time_point now =
          std::chrono::high_resolution_clock::now();
  memcpy (&start, pmt::blob_data (m), sizeof(start));
  d_delay += std::chrono::duration<double>(now - start).count();
  d_cnt++;
}

/*
 * Our virtual destructor.
 */
sink_impl::~sink_impl ()
{
  delete [] d_buffer;
}

bool
sink_impl::stop ()
{
  std::cout << "Port received " << d_cnt << " messages" << std::endl;
  std::cout << "Average delay:" << d_delay/d_cnt << " seconds" << std::endl;
  return true;
}

} /* namespace compare */
} /* namespace gr */

