/* -*- c++ -*- */
/*
 * Copyright 2018, 2019 Manolis Surligas <surligas@gmail.com>
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "stream_sink_impl.h"

namespace gr
{
namespace compare
{

stream_sink::sptr
stream_sink::make (int vlen)
{
  return gnuradio::get_initial_sptr (new stream_sink_impl (vlen));
}

/*
 * The private constructor
 */
stream_sink_impl::stream_sink_impl (int vlen) :
        gr::sync_block ("stream_sink",
                        gr::io_signature::make (1, 1, vlen),
                        gr::io_signature::make (0, 0, 0))
{
}

/*
 * Our virtual destructor.
 */
stream_sink_impl::~stream_sink_impl ()
{
}

int
stream_sink_impl::work (int noutput_items,
                        gr_vector_const_void_star &input_items,
                        gr_vector_void_star &output_items)
{
  const uint8_t *in = (const uint8_t *) input_items[0];

  // Tell runtime system how many output items we produced.
  return noutput_items;
}

} /* namespace compare */
} /* namespace gr */

