/* -*- c++ -*- */
/* 
 * Copyright 2018, 2019 Manolis Surligas <surligas@gmail.com>
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_COMPARE_SINK_IMPL_H
#define INCLUDED_COMPARE_SINK_IMPL_H

#include <compare/sink.h>

#include <vector>

namespace gr
{
namespace compare
{

class sink_impl : public sink
{

public:
  sink_impl (size_t nports, size_t msg_size);
  ~sink_impl ();

  bool
  stop ();

private:
  const size_t d_msg_size;
  uint8_t *d_buffer;
  size_t d_cnt;
  double d_delay;

  void
  handler (pmt::pmt_t m);
};

} // namespace compare
} // namespace gr

#endif /* INCLUDED_COMPARE_SINK_IMPL_H */

