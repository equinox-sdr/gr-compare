/* -*- c++ -*- */
/* 
 * Copyright 2018, 2019 Manolis Surligas <surligas@gmail.com>
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_COMPARE_SOURCE_IMPL_H
#define INCLUDED_COMPARE_SOURCE_IMPL_H

#include <compare/source.h>

namespace gr
{
namespace compare
{

class source_impl : public source
{

public:
  source_impl (size_t nports, size_t msg_size, size_t delayus);
  ~source_impl ();

private:
  const size_t d_nports;
  const size_t d_msg_size;
  const size_t d_delayus;
  size_t d_cnt;
  bool d_running;
  uint8_t *d_buffer;
  boost::shared_ptr<boost::thread> d_thread;

  void
  send_msgs ();
};

} // namespace compare
} // namespace gr

#endif /* INCLUDED_COMPARE_SOURCE_IMPL_H */

