INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_COMPARE compare)

FIND_PATH(
    COMPARE_INCLUDE_DIRS
    NAMES compare/api.h
    HINTS $ENV{COMPARE_DIR}/include
        ${PC_COMPARE_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    COMPARE_LIBRARIES
    NAMES gnuradio-compare
    HINTS $ENV{COMPARE_DIR}/lib
        ${PC_COMPARE_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(COMPARE DEFAULT_MSG COMPARE_LIBRARIES COMPARE_INCLUDE_DIRS)
MARK_AS_ADVANCED(COMPARE_LIBRARIES COMPARE_INCLUDE_DIRS)

